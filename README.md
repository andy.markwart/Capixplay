Project Capixplay
=================

<h2>Maker Challenge 2016 Submission Project</h2>
<h3>Project name:		Capixplay</h3>
<h3>Submission category:	Creative project</h3>
<h3>Project developer: 	Andreas Markwart</h3>
<h3>Co-contributor:		Thi Yen Thu Nguyen</h3>

<h2><b>Description of features</h2>
<h3>Capacitive touch sensors with various, customizable functions</h3>
Currently we have pre-programmed 4 different functions that can be accessed through 4 capacitive touch sensors 
(i.e. by touching the 4 sides of the mood lamp case):
<img src = "/git_images/sensor_buttons.png">

<h3>Various, customizable modes of lighting display</h3>
We have also pre-programmed 8 different modes of display and 1 standby mode on the mood lamp:

<img src = "/git_images/lighting_modes.png">
From left to right, top to bottom: Red light mode, Yellow light mode, Green light mode, Blue light mode, 
Warm white light mode, Cold white light mode, Rainbow animation mode, Heartbeat animation mode, standby mode

The program is fully re-programmable to personalize the sensors and the display to one’s preferences.

<h3>Different standing possibilities</h3>
It is possible to place or mount the lamp in different settings:

<img src = "/git_images/standing_positions.png">
From left to right: Horizontal placement, Slanted vertical standing
It is also possible to mount the lamp on a wall using the screw holes.

<h2><b>Potential extensions of features</h2>
<h3>IoT integration</h3>
One important and powerful extension to the Capixplay project could be attaching an additional WIFI module that would enable the mood lamp to be remotely controlled from different devices supporting WiFi.

It has now come to the time of the Internet of Things trend where home automation is one typical example. With WiFi enabled on both the Capixplay lamp and our smartphones, we can cycle through different modes of lighting or display on the lamp by simply toggling different buttons on a website. To act as a notifier, an integration to various messaging services and social networks would also be possible.

<h3>Retro-style low resolution LED display</h3>
With larger number of Neopixels, it is totally possible to convert the Capixplay lamp into a low resolution LED display that can show a custom photo, a custom string of text, etc. It is also possible that the photo and text strings are sent over WiFi through an attached WiFi module on the lamp and thus the received data will be displayed in real time.

<h2><b>Attached documents</h2>
Below is the list of all documents and files needed to build the project:

- case_design.pdf
- helper_design.pdf
- material_list.pdf
- mood_lamp.ino
- project_description.pdf
- setup_instructions.pdf
- standing_supports.pdf

<h2><b>License and information</h2>
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Capixplay</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/andy.markwart/Capixplay" property="cc:attributionName" rel="cc:attributionURL">Andreas Markwart</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.