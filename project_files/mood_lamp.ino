#include <Adafruit_NeoPixel.h>
#include <CapacitiveSensor.h>

#define menueBtnSensorval 300           // change to ajust sensor sensibility (see serial output numbers and setup instructions)
#define rightBtnSensorval 300
#define leftBtnSensorval 300
#define powerBtnSensorval 700

#define NEO_PIN    4    // digital IO pin connected to the neopixels.
#define NEO_COUNT 35

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NEO_COUNT, NEO_PIN, NEO_GRB + NEO_KHZ800);

CapacitiveSensor   leftBtn = CapacitiveSensor(5,6);   // 1 - 10M ohm resistor between pins 5 & 6, pin 6 is sensor pin, add a foil
CapacitiveSensor   menueBtn = CapacitiveSensor(5,7);  // 1 - 10M ohm resistor between pins 5 & 7, pin 7 is sensor pin, add a foil
CapacitiveSensor   powerBtn = CapacitiveSensor(5,8);  // 1 - 10M ohm resistor between pins 5 & 8, pin 8 is sensor pin, add a foil
CapacitiveSensor   rightBtn = CapacitiveSensor(5,9);  // 1 - 10M ohm resistor between pins 5 & 9, pin 9 is sensor pin, add a foil


int showType = 1;                // <<< set to showType = 0 for neopixels to be off at startup, instead of red
float lum = 1.0;
int animRate = 20;
int lastShowType = 0;
long lastMillisMenue = 0;
long lastMillisLeft = 0;
long lastMillisRight = 0;
long lastMillisPower = 0;

// pixels for the beating hearth animation
const int hearthPixel[10] = {2,4,8,10,12,15,19,23,25,31};



boolean debug = true;           // enables serial output to show the measured data 
                                // off all capacitive sensors


void setup() {
  if(debug) Serial.begin(115200);
  strip.begin();
  strip.show();
}



void loop() {
  getInput();
  startShow(showType);
}






void startShow(int i) {
  switch(i){
    case 0: colorWipe(strip.Color(0, 0, 0), 50);                  // Black/off
            break;
    case 1: colorWipe(strip.Color(255 * lum, 0, 0), 50);          // Red
            break;
    case 2: colorWipe(strip.Color(255 * lum, 155 * lum, 0), 50);  // Yellow
            break;
    case 3: colorWipe(strip.Color(0, 255 * lum, 0), 50);          // Green
            break;
    case 4: colorWipe(strip.Color(0, 0, 255 * lum), 50);          // Blue
            break;
    case 5: colorWipe(strip.Color(255 * lum, 255 * lum, 35 * lum), 50);  // warm white
            break;
    case 6: colorWipe(strip.Color(255 * lum, 255 * lum, 255 * lum), 50);  // cold white
            break;
    case 7: rainbow2(animRate);                                   // rainbow animation
            break;
    case 8: hearth();                                             // beating hearth animation
            break;
  }
}



void getInput() {
  if(debug) {
    Serial.print("menue:");
    Serial.print(menueBtn.capacitiveSensor(30)); 
    Serial.print(" \t");
    Serial.print("left: ");
    Serial.print(leftBtn.capacitiveSensor(30));
    Serial.print(" \t");
    Serial.print("right: ");
    Serial.print(rightBtn.capacitiveSensor(30));
    Serial.print(" \t");
    Serial.print("power: ");
    Serial.print(powerBtn.capacitiveSensor(30));
    Serial.print(" \t");
    Serial.print("lum: ");
    Serial.print(lum);
    Serial.print(" \t");
    Serial.print("showType: ");
    Serial.print(showType);
    Serial.print(" \t");
    Serial.print("animRate: ");
    Serial.println(animRate);
  }


  // Measure all capacitive senors for user inpout


  // Menue button
  if( menueBtn.capacitiveSensor(30) > menueBtnSensorval && millis() - lastMillisMenue > 500) {
    showType++;
    if( showType > 8) {
      showType = 1;
    }
    lastMillisMenue = millis();
  }

  // left button
  if( leftBtn.capacitiveSensor(30) > leftBtnSensorval && millis() - lastMillisLeft > 300) {
    if(showType != 7) {
      lum -= 0.1;
      if( lum < 0.1) {
        lum = 0.1;
      }
    }
    else {
      animRate += 10;
      if(animRate > 100) {
        animRate = 100;
      }
    }
    lastMillisLeft = millis();
  }

  // right button
  if( rightBtn.capacitiveSensor(30) > rightBtnSensorval && millis() - lastMillisRight > 300) {
    if(showType != 7) {
      lum += 0.1;
      if( lum > 1.0) {
        lum = 1.0;
      }
    }
    else {
      animRate -= 5;
      if(animRate < 0) {
        animRate = 0;
      }
    }
    lastMillisRight = millis();
  }
  
  // power button
  if( powerBtn.capacitiveSensor(30) > powerBtnSensorval && millis() - lastMillisPower > 1000) {
    if(showType != 0) {
      lastShowType = showType;
      showType = 0;
    }
    else {
      showType = lastShowType;
    }
    lastMillisPower = millis();
  }
}


// Fill all neopixels with with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
  }
}


// older rainbow animation
void rainbow(uint8_t wait) {
  uint16_t i, j;
  if(animRate != 0)
  {
    for(j=0; j<256; j++) {
      getInput();
        if(showType != 7) 
          break;
      for(i=0; i<strip.numPixels(); i++) {
        strip.setPixelColor(i, Wheel((i+j) & 255));
      }
      strip.show();
      delay(animRate);
    }
  }
  else {      // disco mode
    for(j=0; j<256; j+=30) {
      getInput();
        if(showType !=7) 
          break;
      for(i=0; i<strip.numPixels(); i++) {
        strip.setPixelColor(i, Wheel((i+j) & 255));
      }
      strip.show();
      delay(animRate);
    }  
  }  
}


// rainbow animation
void rainbow2(uint8_t wait) {
  uint16_t i, j;
  for(j=0; j<256; j++) {
    getInput();
      if(showType != 7) 
        break;
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(animRate);
  }
}


// return color
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}


// beating hearth animation
void hearth() {
  for(int i=0; i<NEO_COUNT; i++) {
    strip.setPixelColor(i,strip.Color(0,0,0));
  }
  strip.show();
  
  for( int x=0; x< 256; x++) {
    for(int n=0; n<10; n++) {
      strip.setPixelColor(hearthPixel[n],strip.Color(x,0,0));
    }
    strip.show();
    if (x%20 == 0) {
      getInput();
      if (showType != 8) {
        break;
      }
    }
    else {
      delay(4);
    }
  }
  for( int x=255; x>15; x--) {
    for(int n=0; n<10; n++) {
      strip.setPixelColor(hearthPixel[n],strip.Color(x,0,0));
    }
    strip.show();
    if (x%20 == 0) {
      getInput();
      if (showType != 8) {
        break;
      }
    }
    else {
      delay(4);
    }
  }
  getInput();
}


